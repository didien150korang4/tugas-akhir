          <nav class="mt-2">
            <ul
              class="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
              <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item">
                <a href="index.php?page=dashboard" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                    Dashboard
                    <span class="right badge badge-danger">New</span>
                  </p>
                </a>
              </li>
              <li class="nav-item menu-open">
                 <a href="#" class="nav-link ">
                  <i class="nav-icon fas fa-users"></i>
                  <p>
                    Seksi Pendidikan
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
              
                  <li class="nav-item">
                    <a href="index.php?page=data_santri" class="nav-link <?php if($_GET['page']=='data_santri'){echo 'active';}?>">
                      <i class=" nav-icon fas fa-book"></i>
                      <p>Data Santri </p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="index.php?page=data_guru" class="nav-link <?php if($_GET['page']=='data_guru'){echo 'active';}?>">
                      <i class="nav-icon fas fa-book"></i>
                      <p>Data Guru</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="index.php?page=data_kegiatan" class="nav-link <?php if($_GET['page']=='data_kegiatan'){echo 'active';}?>">
                      <i class="nav-icon fas fa-book"></i>
                      <p>Data Kegiatan</p>
                    </a>
                  </li>
                      <li class="nav-item">
                    <a href="index.php?page=penjadwalan" class="nav-link <?php if($_GET['page']=='penjadwalan'){echo 'active';}?>">
                      <i class="nav-icon fas fa-calendar"></i>
                      <p>Penjadwalan </p>
                    </a>
                  </li>
                      <li class="nav-item">
                    <a href="index.php?page=absensi" class="nav-link <?php if($_GET['page']=='absensi'){echo 'active';}?>">
                      <i class="nav-icon fas  fa-building"></i>
                      <p>Absensi</p>
                    </a>
                  </li>
                      <li class="nav-item">
                    <a href="index.php?page=pelanggaran" class="nav-link <?php if($_GET['page']=='pelanggaran'){echo 'active';}?>">
                      <i class=" nav-icon fas fa-calendar-times"></i>
                      <p>Pelanggaran </p>
                    </a>
                  </li>
                </ul>
              </li>
               <li class="nav-item">
                <a href="logout.php" class="nav-link text-red">
                  <i class="nav-icon fas fa-power-off"></i>
                  <p>
                    Logout
                  </p>
                </a>
              </li>
            </ul>
            <div class="card text-center bg-secondary">
                <div class="card-block">
                    <i class="feather icon-sunset f-40"></i>
                    <h6 class="mt-3">SMKS</h6>
                    <h6>Copyright © 2023</h6>
                    <p>Designed & Developed by <a>zaifuddin.rf.gd</a></p>
                </div>
              </div>
          </nav>