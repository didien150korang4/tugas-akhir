    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <!-- <h3 class="card-title"> Data Santri </h3> -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-lg">
                  Tambah Data
                </button>
                <br></br>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Nomor Induk Santri</th>
                    <th>Kelas</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php if($_GET['page']=='data_santri'){echo 'active';}?>
                    <?php
                    $no = 0;
                    $query =mysqli_query($koneksi, "SELECT * FROM tb_santri");
                    while($santri = mysqli_fetch_array($query)){
                      $no++
                    ?>
                  <tr>
                    <td width="5%"><?php echo $no;?></td>
                    <td><?php echo $santri['nama'];?></td>
                    <td><?php echo $santri['nis'];?></td>
                    <td><?php echo $santri['kelas'];?></td>
                    <td>
                      <a onclick="hapus_data(<?php echo $santri['id'];?>)" class="btn btn-sm btn-danger">Hapus</a>
                      <a href="index.php?page=edit-data&& id=<?php echo $santri['id'];?>" class="btn btn-sm btn-success">Edit</a>
                      <a class="view-data btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-view"
                       data-nama="<?php echo $santri['nama'];?>"
                       data-nis="<?php echo $santri['nis'];?>"
                      data-kelas="<?php echo $santri['kelas'];?>">View Data</a>
                    </td>
                  </tr>
                  <?php }?>
                  </tbody>
                  <tfoot>
                  <!-- <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade9</th>
                  </tr> -->
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
              <div class="modal fade" id="modal-lg">
                      <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                    <div class="modal-header">
                                              <h4 class="modal-title">Tambah Data</h4>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                          </div>
                                          
                                            <form method="post" action="add/tambah_data_santri.php">
                                                <div class="modal-body">
                                                  <div class="form-row">
                                                    <div class="col">
                                                      <input type="text" class="form-control" placeholder="Nama" name="nama" required>
                                                    </div>
                                                    <div class="col">
                                                      <input type="text" class="form-control" placeholder="Nomor Induk" name="nis" required>
                                                    </div>
                                                    <div class="col">
                                                      <select class="custom-select" id="inputGroupSelect01" name="kelas" required>
                                                        <option selected>Pilih...</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                              <!-- <p>One fine body&hellip;</p> -->
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                              <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                                              <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                          </div>
                                          </form>

                                          <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                      </div>
  <!--------- modal vie ---------->

                                      <div class="modal fade" id="modal-view">
                                        <div class="modal-dialog modal-lg">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h4 class="modal-title">View Data</h4>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <form method="get" action="data_santri.php">
                                            <div class="modal-body">
                                                  <div class="form-row">
                                                    <div class="col">
                                                      Nama : <span id="nama"></span>
                                                    </div>
                                                    <div class="col">
                                                      Nomor Induk Santri : <span id="nis"></span>
                                                    </div>
                                                    <div class="col">
                                                      Kelas : <span id="kelas"></span>
                                                    </div>
                                                  </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                              <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                                              <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                          </div>
                                          </form>

                                          <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                      </div>

<script>
  function hapus_data(data_id){
    //window.location=("delete/hapus_data.php?id="+data_id);
    Swal.fire({
        title: 'Apakah Datanya akana Dihapus?',
        // showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Hapus Data',
        confirmButtonColor: '#cd5c5c',
        // denyButtonText: `Don't save`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          window.location=("delete/hapus_data.php?id="+data_id);
        } 
      })
  }
</script>