<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">DataTable with default features</h3>
          </div>
          <div class="card-body">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-lg">
                  Tambah Data
                </button>
                <br></br>
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Guru</th>
                  <th>No HP</th>
                  <th>Tingkat</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                <?php
                $no = 0;
                $query = mysqli_query($koneksi, "SELECT * FROM tb_guru");
                while ($guru = mysqli_fetch_array($query)) {
                  $no++;
                ?>
                  <tr>
                    <td width="5%"><?php echo $no; ?></td>
                    <td><?php echo $guru['nama_guru']; ?></td>
                    <td><?php echo $guru['no_handpon']; ?></td>
                    <td><?php echo $guru['tingkatan_mengajar']; ?></td>
                    <td>
                      <a onclick="hapus_data(<?php echo $guru['id']; ?>)" class="btn btn-sm btn-danger">Hapus</a>
                      <a href="index.php?data_guru&& id=<?php echo $guru['id']; ?>" class="btn btn-sm btn-success">Edit</a>
                      <a class="view-data btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-view"
                        nama-guru="<?php echo $guru['nama_guru']; ?>"
                        no-handpon="<?php echo $guru['no_handpon']; ?>"
                        tingkatan-mengajar="<?php echo $guru['tingkatan_mengajar']; ?>">View Data</a>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama Guru</th>
                  <th>No HP</th>
                  <th>Tingkat</th>
                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Guru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="add/tambah_data_guru.php">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nama_guru">Nama Guru:</label>
                        <input type="text" class="form-control" id="nama_guru" name="nama_guru" required>
                    </div>
                    <div class="form-group">
                        <label for="no_handpon">Nomor HP:</label>
                        <input type="text" class="form-control" id="no_handpon" name="no_handpon" required>
                    </div>
                    <div class="form-group">
                        <label for="tingkatan_mengajar">Tingkatan Mengajar:</label>
                        <input type="text" class="form-control" id="tingkatan_mengajar" name="tingkatan_mengajar" required>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
  function hapus_data(data_id) {
    Swal.fire({
      title: 'Apakah Data ini Akan Dihapus?',
      showCancelButton: true,
      confirmButtonText: 'Hapus Data',
      confirmButtonColor: '#cd5c5c',
    }).then((result) => {
      if (result.isConfirmed) {
        window.location = "delete/hapus_data.php?id=" + data_id;
      }
    })
  }
</script>
